## Quick Start

Install Node.js, clone this repo and then:

```sh
$ cd client/
$ sudo npm -g install grunt-cli karma bower
$ npm install
$ bower install
$ grunt watch
```
# README #

A starting-point project for developing client/service architecture web applications for Google App Engine, using Python for the server-side and Javascript for the client-side.

Prominent technologies are Flask RESTful, and AngularJS.

## Quick Start Guide (Linux) ##
(assuming you're in your project directory in the terminal window)

```
#!sh

git clone git@bitbucket.org:bogdanvarlamov/gae-ng-bootstrap.git
cd gae-ng-bootstrap

```

### JS Client Setup ###

```
#!sh

cd client
sudo npm -g install grunt-cli karma bower
sudo npm install
bower install
grunt watch

```
Open the build/index.html file in your web browser, if you can see [] PUT YOUR CONTENT HERE then everything is ready on the client side.


### Server Setup ###

```
#!sh

cd server
pip install -r requirements_dev.txt -t src/lib
python src/application/generate_keys.py 
dev_appserver.py src/

```
Open your browser to http://localhost:8000 and verify there is a running instance listed there.

### Tying it all together ###
```
#!sh

cd client
grunt
cd ../server/
dev_appserver.py src/

```
NOTE: When you run grunt, it does a build of the JS web client app and copies it to the correct directories in the server for deployment. When you run the server, it loads that app in response to browser requests.

Open browser to http://localhost:8080/ (it should execute the JS client and show you the same page as you saw in the client setup)

### What about SEO HTML prerendering for complex JS? ###
* Open the Gruntfile.js
* Find this text: '[]your URL goes here' and replace the URL of the live (deployed) site you want to crawl and generate pre-rendered HTML for.
* Modify other parameters as you deem necessary
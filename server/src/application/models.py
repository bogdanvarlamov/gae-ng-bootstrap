"""
models.py

App Engine datastore models

break these out into seperate files?

"""
from google.appengine.ext import ndb


class UserModel(ndb.Model):
    email = ndb.StringProperty(required=True)
    facebook_id = ndb.StringProperty()
    google_id = ndb.StringProperty()
    linkedin_id = ndb.StringProperty()
    #github_id = db.Column(db.String(120))
    #twitter_id = db.Column(db.String(120))

    def to_json(self):
        return dict(id=self.key.id(), email=self.email)

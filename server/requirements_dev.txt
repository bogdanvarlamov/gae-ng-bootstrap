# Development environment pip requirements file
# Install via 'pip install -r requirements_dev.txt -t src/lib'
# ONLY run this once!! see: https://github.com/GoogleCloudPlatform/appengine-flask-skeleton/issues/1

Flask
Flask-RESTful
flask_debugtoolbar
git+https://github.com/bogdanvarlamov/gae_mini_profiler.git@pip_support#egg=gae_mini_profiler
Pillow
jinja2
simplejson
unittest2
MarkupSafe
Werkzeug
itsdangerous
PyJWT
requests
requests-oauthlib
google-api-python-client
